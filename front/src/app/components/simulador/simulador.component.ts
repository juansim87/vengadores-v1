import { FormGroup, FormBuilder } from '@angular/forms';
import { CharacterInterface } from 'src/app/models/character.interface';
import { CharacterService } from './../../services/character.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-simulador',
  templateUrl: './simulador.component.html',
  styleUrls: ['./simulador.component.scss']
})
export class SimuladorComponent implements OnInit {
  public callCharacter: CharacterInterface[] = [];
  public characterForm!: FormGroup;
  public selectedCharacter: any[] = [];

  constructor(private router: Router, private characterService: CharacterService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.characterService.getCharacter().subscribe((data:any) => {
      this.callCharacter = data;
      console.log(this.callCharacter);
    });
    this.characterForm = this.formBuilder.group({
      heroOption: [""]
    });
    // this.characterForm.valueChanges.subscribe((changes) => {
    //   console.log(changes);
    // });
    //Observables: cuando hablamos de subscribe hay dos partes importantes, el objeto observable y la suscripción a ese objeto.
    this.characterForm.get("heroOption")?.valueChanges.subscribe((changes) => {
      console.log(changes);
      console.log(this.callCharacter.find(character => character.id.toString() === changes.toString()));
    //Si encuentra el personaje detro del array de personajes

      if (this.selectedCharacter.length < 3) {
        this.recoverHero(this.callCharacter, this.selectedCharacter, changes);
      }

    });
  }
  public deleteSelectedCharacter(id:string) {
    this.recoverHero(this.selectedCharacter, this.callCharacter, id)
  }

  private recoverHero(array1: any[], array2: any[], id: string) {
    //Añadir una condición al if para que evite que entre en el if si el array1 es =< 3

    if (array1.find(character => character.id.toString() === id.toString())) {
      //Guardamos en la variable character el personaje
      const character:any = array1.find(character => character.id.toString() === id.toString());
      //Buscamos en el array el índice del personaje
      const indice = array1.indexOf(character);
      //Guarda el personaje en el array nuevo
      array2.push(character);
      //Elimina el personaje del array viejo
      array1.splice(indice, 1);
      console.log(array2);
      console.log(array1);
    }
  }
}
